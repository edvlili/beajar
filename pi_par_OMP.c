#include <stdio.h>
#include <time.h>
#include <omp.h>
#define NUMSTEPS 1000000000

int main (int argc, char* argv[]) {
            
    double step, x, pi, sum = 0.0;
    int nt;
    
    x=0;
    sum = 0.0;
    step = 1.0/(double) NUMSTEPS;
    
    time_t start_time, end_time;
    
    printf("Number of threads? \n");
    scanf("%d" ,&nt);

    omp_set_num_threads(nt);

    start_time = time(NULL);
    
    #pragma omp parallel
    {
        long int i,inicio,final;
        int yo;
        double x=0;
        double aux=0.0;
        
        yo = omp_get_thread_num();
        
        inicio= yo*(NUMSTEPS/nt);
        
        if(yo != nt-1)
            final=(yo+1)*(NUMSTEPS/nt);
        else
            final=NUMSTEPS;
        
        for (i=inicio; i<final; i++){
            x=(i+0.5)*step;
            aux=aux+4.0/(1.0+x*x);
            
        }
        #pragma omp critical
            sum = sum + aux;
    }
    pi=step*sum;

    end_time = time(NULL);
    printf("pi with %d steps is %f in %d seconds \n", NUMSTEPS, pi, (int) difftime(end_time, start_time));
}	  

